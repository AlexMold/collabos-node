require('./config/config');

const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const { generateMessage, generateLocationMessage } = require('./utils/message');
const { isRealString, userExist } = require('./utils/validation');
const { Users } = require('./utils/users');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT;

const moment = require('moment');

const app = express();

/* Создаем http-сервер для использования его вместе с socket.io */
const server = http.createServer(app);
const io = socketIO(server);

const users = new Users();

app.use(express.static(publicPath));

/* Событие при подключении клиента к серверу */
io.on('connection', (socket) => {

    socket.on('join', (params = { name: 'test', email: 'some@some.com' }, callback) => {
        console.log(' join ')
            // Required Namespace
        socket.param_user = params;

        if (!isRealString(params.name) || !isRealString(params.email)) {
            return callback('Name and Email is required!!!'); // send to client-side
        }

        // Set status for existing user or add new
        users.toggleUser(socket.id, params.name, params.email);


        /* Update all chats */
        io.emit('updateUserList', users.getUserList());

        /* Broadcast to this socket */
        socket.emit('newMessage', generateMessage('Admin', 'Welcome to the Chat!'));

        /* Броадкаст сообщения всем в определенной комнате, кроме этого сокета */
        socket.broadcast.emit('newMessage', generateMessage('Admin', `Connected user - ${params.name}`));

        callback();
    });

    /* Новое сообщение */
    socket.on('createMessage', (message, callback) => {
        var user = users.getUser(message.from, message.email);

        if (user && isRealString(message.text)) {
            /* Броадкаст сообщения всем в определенной комнате */
            io.emit('newMessage', generateMessage(user.name, message.text));
        }

        callback();
    });

    socket.on('createMessageTo', ({ from, to, text }, callback) => {
        console.log(' createMessageTo ', { from, to, text })
        const Recipient = users.getUser(to.name, to.email)
        const RecipientID = Recipient ? Recipient.id : null;
        const conversation = users.addMessageAtConversation(from, to, text, moment(moment().valueOf()).format('H:mm:ss'));
        const existRecipientInRoom = io.sockets.adapter.sids[RecipientID][conversation.room];

        socket.emit('newMessageTo', generateMessage(from.name, text));
        existRecipientInRoom ? socket.broadcast.to(RecipientID).emit('haveNewMessageOnlySound', from) : socket.broadcast.to(RecipientID).emit('haveNewMessage', from);

        io.in(conversation.room).emit('updateChat', conversation)

    });

    socket.on('toggleChat', ({ from, to }, callback) => {
        const newConversation = users.selectConversation(from.email, to.email)
        const getAuthor = users.getUser(from.name, from.email);
        const getAuthorID = getAuthor ? getAuthor.id : null;
        const getRecipient = users.getUser(to.name, to.email);
        const getRecipientID = getRecipient ? getRecipient.id : null;

        console.log(' getAuthorID ', getAuthorID);
        console.log(' getRecipientID ', getRecipientID);
        console.log(' newConversation ', newConversation);

        socket.join(newConversation.room);

        socket.emit('updateChat', newConversation)

        callback();
    });



    /* Client has been disconnected */
    socket.on('disconnect', () => {
        console.log(' disconnect ')
        const user = users.toggleUser(socket.id, socket.param_user.name, socket.param_user.email);
        console.log(' socket.user_name ', socket.param_user)

        socket.param_user = null;
        socket.leave();

        if (user) {
            /* Send all in room */
            socket.broadcast.emit('updateUserList', users.getUserList());
            socket.broadcast.emit('newMessage', generateMessage('Admin', `${user.name} leave the chat`));
        }
    });
});

server.listen(port, () => {
    console.log(`Сервер работает на порту ${port}`);
});