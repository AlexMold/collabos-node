const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UsersSchema =
    new Schema({
        socketId: { type: String, index: true },
        name: { type: String, index: true },
        email: { type: String, index: true },
        status: { type: Boolean }
    });

// UsersSchema.methods.getUserByNameAndEmail = function(name, email) {
//     const User = this.model('User');

//     return User.findOne({
//             name,
//             email,
//         })
//         .then(user => {
//             if (!user) {
//                 return Promise.reject(new Error('User doesn\'t found!'));
//             }
//             return Promise.resolve(user);
//         })
// };


const MessageSchema =
    new Schema({
        from: { type: String, index: true },
        to: { type: String, index: true },
        text: { type: String },
        createdAt: Schema.Types.Mixed
    });


const ConversationSchema =
    new Schema({
        participants: [String],
        room: { type: String, index: true },
        messages: [MessageSchema]
    });








class Storage {
    static getAdapter() {
        let adapter = new Storage('mongodb://127.0.0.1:27018/socket_chat');
        return adapter;
    }

    static UsersSchema() {
        return UsersSchema;
    }

    static ConversationSchema() {
        return ConversationSchema;
    }

    constructor(dbSettings) {
        this.conn = mongoose.connect(dbSettings);

        this.User = mongoose.model('User', UsersSchema)
        this.Conversation = mongoose.model('Conversation', ConversationSchema)
    }

    addUser(socketId, name, email) {
        let user = new this.User({
            socketId,
            name,
            email
        })

        return user.save().then(user => {
            return Promise.resolve(user);
        })
    }

    getUserList() {
        return this.User
            .find()
            .then(users => {
                return Promise.resolve(users)
            })
    }

    getUserByNameAndEmail(name, email) {

        return this.User.findOne({
                name,
                email,
            })
            .then(user => {
                if (!user) {
                    return Promise.reject(new Error('User doesn\'t found!'));
                }
                return Promise.resolve(user);
            })
    }

}


module.exports = Storage;