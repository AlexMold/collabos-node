const isRealString = (string) => {
    return (typeof string === 'string') && (string.trim().length > 0);
};

const userExist = (fn, { name, email }) => {
    return fn.userExist(name, email).length ? true : false;

}

module.exports = { isRealString, userExist };