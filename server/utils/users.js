const Storage = require('../storage.js');


class Users {
    constructor() {
        // const storage = Storage.getAdapter();

        // this.store = storage;
        this.users = [];
        this.conversation = [];

    }

    addUser(id, name, email, cb) {
        // this.store.addUser(id, name, email)
        //     .then(user => {
        //         // console.log(' addUser ', addUser);
        //         cb(user);
        //         return user;
        //     });

        // console.log(' newUserFromDB ', newUserFromDB)

        const user = { id, name, email, status: true };
        this.users.push(user);

        return user;
    }

    toggleUser(id, name, email) {
        // this.getUser(name, email, (user, err) => {
        //     console.log(' toggleuser cb user ', user)
        //     console.log(' toggleuser cb error ', err)
        // })
        let user = this.getUser(name, email);
        // console.log(' toggleUser ', user)
        if (user) {
            this.users = this.users.map(user => {
                if (user.name === name && user.email === email) {
                    user.status = !user.status
                        // Update ID for user broadcast
                    user.id = id
                }
                return user;
            })
        }

        if (!user) {

            let user = this.addUser(id, name, email)
        }

        return user;

    }

    selectConversation(from, to) {
        let newConversation = this.conversation
            .filter(conv => conv.participants.indexOf(from) != -1 && conv.participants.indexOf(to) != -1)[0];

        console.log(' selectConversation newConversation ', newConversation)

        if (!newConversation) {
            newConversation = { participants: [from, to], room: `${from}-${to}`, messages: [] };
            this.conversation.push(newConversation)
        }



        return newConversation;
    }

    addMessageAtConversation(from, to, message, createdAt) {
        const newMessage = { from: from.name, to: to.name, text: message, createdAt: createdAt }
        this.conversation = this.conversation.map(conv => {
            if (conv.participants.indexOf(from.email) != -1 && conv.participants.indexOf(to.email) != -1) {
                conv.messages.push(newMessage);
            }
            return conv;
        })

        console.log(' addMessageAtConversation this.conversation ', this.conversation);
        return this.selectConversation(from.email, to.email);
    }

    getUser(name, email, cb) {
        // const getUserByNameAndEmail = this.store.getUserByNameAndEmail(name, email)
        //     .then(user => {
        //         cb(user, null)
        //     })
        //     .catch(err => {
        //         cb(null, err)
        //     })

        // console.log(' getUserByNameAndEmail ', getUserByNameAndEmail);

        return this.users
            .filter((user) => user.name === name && user.email === email)[0];
    }

    userExist(name, email) {
        return this.users
            .filter((user) => user.email === email || user.name === name);
    }

    getUserList() {
        console.log(' getUserList ', this.users)
        console.log(' conversation ', JSON.stringify(this.conversation))
        return this.users;
    }
}

module.exports = { Users };