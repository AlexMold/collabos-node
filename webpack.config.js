const path = require('path');
const webpack = require('webpack');
const AssetsPlugin = require('assets-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development'

module.exports = {
    entry: {
        main: './public/main.js',
    },

    output: {
        path: path.resolve(__dirname, './public/'),
        publicPath: './public/',
        filename: '[name].bundle.js',
    },

    resolve: {
        extensions: ['.js'],
    },

    watch: true,

    plugins: [],

    module: {
        loaders: [{
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url',
            },
        ],
    }
};


if (NODE_ENV == 'production') {

    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            sourceMap: false,
            mangle: false,
            compress: {
                warnings: false
            }
        })
    );

    module.exports.watch = false;
};