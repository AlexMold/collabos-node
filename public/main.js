import Vue from "vue/dist/vue.js";
import moment from 'moment';
const socket = io();


const vm = new Vue({
    el: '#app',

    data: {
        users: [],
        conversations: [{
            room: 'main',
            messages: []
        }],
        input: '',
        messageTo: {},
        currentMessages: []
    },

    ready: function() {
        this.log();
    },

    methods: {

        log: function(data) {
            console.log(' start ', data)
        },

        updateUserList: function(users) {
            this.users = users.map(user => {
                user.haveNewMessage = false;
                return user;
            });
            console.log(' users ', this.users)
        },

        sendMessage: function(e) {
            const params = $.deparam(window.location.search);
            e.preventDefault();

            if (this.messageTo.name && this.messageTo.email) {

                socket.emit('createMessageTo', {
                    from: params,
                    to: this.messageTo,
                    text: this.input.trim()
                });

                this.input = '';

            } else {

                socket.emit('createMessage', {
                    from: params.name,
                    text: this.input.trim(),
                    email: params.email
                }, () => {
                    this.input = ''; // Clear input field
                });

            }

        },

        getMessage: function({ text, from, createdAt, room }) {
            console.log(' get message ', { text, from, createdAt, room })

            this.conversations.forEach((element) => {
                if (element.room === room) {
                    element.messages.push({ text, from, createdAt })
                }
            });

            this.currentMessages.push({ text, from, createdAt });
        },

        _addMessageTo: function(choosedUser) {
            console.log(' _addMessageTo ', choosedUser)
            this.messageTo = choosedUser;
        },

        toggleChat: function({ name, email, haveNewMessage }) {
            const params = $.deparam(window.location.search);
            console.log(' toggleChat ', { name, email });
            haveNewMessage ? this.toggleHaveNewMessage({ name, email }) : '';

            // Check if not owner
            if (params.name != name && params.email != email) {
                socket.emit('toggleChat', {
                    from: params,
                    to: { email, name }
                }, () => {
                    this._addMessageTo({ email, name });
                })
            }
        },

        updateChat: function(conversation) {
            console.log(' updateChat ', conversation)
            const checkIfConversationExists = this.conversations.filter(vueItem => vueItem.room === conversation.room).length;

            if (!checkIfConversationExists) {
                this.conversations.push(conversation);
            } else {
                this.conversations.forEach((element) => {
                    if (element.room === conversation.room) {
                        element.messages = conversation.messages
                    }
                });
            }

            this.currentMessages = conversation.messages;
        },

        toggleHaveNewMessage: function(from) {
            this.users.forEach(user => {
                if (user.name === from.name && user.email === from.email) {
                    user.haveNewMessage = !user.haveNewMessage;
                }
            })
        }

    }
})

function scrollToBottom() {
    var messages = $('#messages');
    var newMessage = messages.children('li:last-child');

    var clientHeight = messages.prop('clientHeight');
    var scrollTop = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');
    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight();

    if ((clientHeight + scrollTop + newMessageHeight + lastMessageHeight) >= scrollHeight) {
        messages.scrollTop(scrollHeight);
    }
}

/* Событие при подключении к серверу */
socket.on('connect', function() {
    var params = $.deparam(window.location.search); // парсим параметры из урла

    socket.emit('join', params, function(err) {
        if (err) {
            alert(err);
            window.location.href = '/';
        } else {
            console.log('ok');
        }
    });
});

/* Событие при отключении от сервера */
socket.on('disconnect', function() {
    console.log('Disconnected from server');
});

/* Обновление списка юзеров */
socket.on('updateUserList', function(users) {
    vm.updateUserList(users);
});



/* Кастомное событие от сервера */
socket.on('newMessage', function(message) {
    const formattedTime = moment(message.createdAt).format('H:mm:ss');
    const params = $.deparam(window.location.search);

    const formattedMessage = {
        text: message.text,
        from: message.from,
        createdAt: formattedTime,
        room: 'main'
    }

    vm.getMessage(formattedMessage);
    scrollToBottom();
});

// /* Кастомное событие от сервера */
// socket.on('newMessageTo', function(message) {
//     var formattedTime = moment(message.createdAt).format('H:mm:ss');
//     var template = $('#message-template').html();
//     var params = $.deparam(window.location.search);

//     var html = Mustache.render(template, {
//         text: message.text,
//         from: message.from,
//         createdAt: formattedTime
//     });

//     $('#messages').append(html);
//     scrollToBottom();
// });


// /* Отправка сообщения */
// $('#message-form').on('submit', function(e) {
//     e.preventDefault();
//     var form = $(e.target);
//     var params = $.deparam(window.location.search);
//     var emailRecipient = form.attr('data-recipient-email');
//     var nameRecipient = form.attr('data-recipient-name');
//     var messageTextbox = $('[name=message]');


//     if (emailRecipient && nameRecipient) {
//         socket.emit('createMessageTo', {
//             from: params,
//             to: { email: emailRecipient, name: nameRecipient },
//             text: messageTextbox.val()
//         }, function() {
//             messageTextbox.val(''); // очищаем поле ввода
//         });
//     } else {
//         socket.emit('createMessage', {
//             from: params.name,
//             text: messageTextbox.val(),
//             email: params.email
//         }, function() {
//             messageTextbox.val(''); // очищаем поле ввода
//         });
//     }


// });

// // Toggle Chat
// $('body').on('click', '#users li', function(e) {
//     e.preventDefault()

//     var params = $.deparam(window.location.search);
//     var el = $(e.target);
//     var email = el.attr('data-email');
//     var name = el.attr('data-name');
//     var messageForm = $('#message-form');


//     console.log(' messageTextbox ', messageForm)
//     console.log(' el ', el)

//     if (email !== params.email) {

//         el.removeClass('is-active');

//         messageForm.attr('data-recipient-email', email);
//         messageForm.attr('data-recipient-name', name);

//         socket.emit('toggleChat', {
//             from: params,
//             to: { email, name }
//         })

//     }

//     console.log(' el ', el.data('email'))
// })

socket.on('haveNewMessage', function(from) {
    let audio = new Audio('./stuffed-and-dropped.mp3');
    audio.play();
    vm.toggleHaveNewMessage(from);
})

socket.on('haveNewMessageOnlySound', function(from) {
    let audio = new Audio('./stuffed-and-dropped.mp3');
    audio.play();
})

socket.on('updateChat', function(conversation) {
    vm.updateChat(conversation);
})