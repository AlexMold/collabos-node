# Express + SocketIO + Mongoose(added but not implemented) + Vue.js(without vue-loader)



## Installation

```sh
npm install
npm run build
npm run start
```

## Installation for dev ENV

```sh
npm install
npm run static-dev
npm run start-dev
```


## Tests

```sh
npm install
npm test
```

## Dependencies

- [express](https://github.com/expressjs/express): Fast, unopinionated, minimalist web framework
- [moment](https://github.com/moment/moment): Parse, validate, manipulate, and display dates
- [mongodb](https://github.com/mongodb/node-mongodb-native): The official MongoDB driver for Node.js
- [mongoose](https://github.com/Automattic/mongoose): Mongoose MongoDB ODM
- [socket.io](https://github.com/socketio/socket.io): node.js realtime framework server
- [vue](https://github.com/vuejs/vue): Reactive, component-oriented view layer for modern web interfaces.

## Dev Dependencies

- [assets-webpack-plugin](https://github.com/sporto/assets-webpack-plugin): Emits a json file with assets paths
- [autoprefixer](https://github.com/postcss/autoprefixer): Parse CSS and add vendor prefixes to CSS rules using values from the Can I Use website
- [babel-core](https://github.com/babel/babel/tree/master/packages): Babel compiler core.
- [babel-loader](https://github.com/babel/babel-loader): babel module loader for webpack
- [babel-preset-es2015](https://github.com/babel/babel/tree/master/packages): Babel preset for all es2015 plugins.
- [babel-preset-stage-0](https://github.com/babel/babel/tree/master/packages): Babel preset for stage 0 plugins
- [expect](https://github.com/mjackson/expect): Write better assertions
- [extract-text-webpack-plugin](https://github.com/webpack/extract-text-webpack-plugin): Extract text from bundle into a file.
- [mocha](https://github.com/mochajs/mocha): simple, flexible, fun test framework
- [nodemon](https://github.com/remy/nodemon): Simple monitor script for use during development of a node.js app.
- [webpack](https://github.com/webpack/webpack): Packs CommonJs/AMD modules for the browser. Allows to split your codebase into multiple bundles, which can be loaded on demand. Support loaders to preprocess files, i.e. json, jsx, es7, css, less, ... and your custom stuff.
- [webpack-cleanup-plugin](https://github.com/gpbl/webpack-cleanup-plugin): Plugin for webpack to cleanup extraneous files from the output path directory


## Disclaimer

DB Schemas and a few methods was developed, but not implemented.
Because of need refactor for async requests(Promise).
Now all info keeps in ./server/utils/user.js instance.
Also, for production ready need
- [PassportJS](https://github.com/jaredhanson/passport): Passport is Express-compatible authentication middleware for Node.js.

I think:

- [MVP](https://en.wikipedia.org/wiki/Minimum_viable_product) - is ready!!!


## License

ISC
